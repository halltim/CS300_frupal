
from item import Items
from tile import Tiles
import json

def loadConfig(state):
	try:
		with open("config.txt", "r") as file:
			total = file.read()
			total = json.loads(total)
			#replace defaults with config
			state.tiles.terrain = total["terrain"]
			state.tiles.obstacles = total["obstacles"]
			state.items.item = total["items"]
			state.x_dim = total["x_dim"]
			state.y_dim = total["y_dim"]
			state.config_energy = total["energy"]
			state.config_money = total["money"]
			state.intro_flag = total["intro_flag"]
	except FileNotFoundError:
		return False

def saveConfig(state):
	total = {
		"terrain": state.tiles.terrain,
		"obstacles": state.tiles.obstacles,
		"items": state.items.item,
		"x_dim": state.x_dim,
		"y_dim": state.y_dim,
		"energy": state.config_energy,
		"money": state.config_money,
		"intro_flag": state.intro_flag
	}
	f = open("config.txt", "w")
	f.write(json.dumps(total, indent=4))
	f.close()
	
