class Entity:
	x = 0
	y = 0
	
	def __init__(self, id, name, chance):
		self.id = id + 1
		self.name = name
		self.chance = chance

def addEntity(state, name, chance):
	assert(chance <= 1.0 and chance >= 0.0)
	state.total_entity_chance += chance
	assert(state.total_entity_chance <= 1)
	entity = Entity(len(state.entity_manifest), name, chance)
	state.entity_manifest.append(entity)

def hasEntityAt(entities, x, y):
	for entity in entities:
		if entity.x == x and entity.y == y:
			return True
	return False

def getEntityAt(entities, x, y):
	for entity in entities:
		if entity.x == x and entity.y == y:
			return entity
	return []
	
def removeEntityAt(entities, x, y):
	for index in range(len(entities)):
		entity = entities[index]
		if entity.x == x and entity.y == y:
			del entities[index]
			return

def findFirstEntity(entities, name):
	for entity in entities:
		if entity.name == name:
			return entity

def entityExists(entities, name):
	for entity in entities:
		if entity.name == name:
			return True
	return False
