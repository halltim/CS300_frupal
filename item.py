class Items:
	def __init__(self):
		self.item = {
			0: {'name': 'Power Bar', 'cost': 10, 'obst': 0, 'slot': 1, 'stackable': True},
			1: {'name': 'Binoculars', 'cost': 30, 'obst': 0, 'slot': 0, 'stackable': False},
			2: {'name': 'Magic Locator', 'cost': 100, 'obst': 0, 'slot': 0, 'stackable': False},
			3: {'name': 'Weed Whacker', 'cost': 20, 'obst': 1, 'slot': 0, 'stackable': False},
			4: {'name': 'Jack Hammer', 'cost': 20, 'obst': 3, 'slot': 0, 'stackable': False},
			5: {'name': 'Chain Saw', 'cost': 20, 'obst': 2, 'slot': 0, 'stackable': False},
			6: {'name': 'Boat', 'cost': 50, 'obst': 0, 'slot': 0, 'stackable': False}
		}
		#Cost: index of the obstacle for which this item will be attached to (if it is a tool). See "dealWith" in user.py to see how it is used
		#Slot: index of the user inventory array in user.py to find it later
	 	#Stackable: More than one of this object can be owned in the inventory


	def addItem(state, name, cost, connection, order, stackable=False):
		state.item[len(state.item)]={'name': name, 'cost': cost, 'obst': connection, 'slot': order, 'stackable': stackable}
	

# connect obstacles and objects
	def connectItem(self, item, obstacle):
		self.item[item]['obst']= obstacle

	def getSlot(self, item_name):
		for id in self.item:
			if item_name == self.item[id]['name']:
				return self.item[id]['slot']
		return -1
	
	def setSlot(self, item_name, slot):
		for id in self.item:
			if item_name == self.item[id]['name']:
				self.item[id]['slot'] = slot
