# Frupal
### Team B's frupal project for CS300

## About
 Frupal is a python3 adventure game for the terminal. Your character has awoken on an island and is told to find the jewl. You can purchase tools and supplies to help you on your journey. You have a limited amount of energy so you have to find them quickly. you can purchase power bars for more energy and save energy using tools to remove obsticles. There is 'greedy tile' somewhere on the island that will drain extra energy from you so watch out!
## Installation
Simply download all files into a directory on computer.
 ## Running Game
 Requires python3 to run. From the directory the game is saved, simply run: <br>
    `python3 main.py`<br>
No other dependancies are required.
## Game Controls
- Movement: A,W,S,D
- Eat power bar: e
- Open shop menu: p
- Use magic locator: l
- Instantly win: v
## Configuration
In frupal the user can configure:
- Map size
- Character starting stats
- create new items
- create new obsticals and link them to items
