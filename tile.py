class Tiles:

	def __init__(self):

		self.terrain = {
			0: {'name': 'invalid', 'ascii': '?', 'energy': 0},
			1: {'name': 'grass', 'ascii': '.', 'energy': 1},
			2: {'name': 'forest', 'ascii': 'f', 'energy': 2},
			3: {'name': 'water', 'ascii': '~', 'energy': 3}
		}

		self.obstacles = {
			0: {'name': 'null', 'ascii': '?', 'energy': 0},
			1: {'name': 'bush', 'ascii': '#', 'energy': 2},
			2: {'name': 'tree', 'ascii': 'T', 'energy': 3},
			3: {'name': 'rock', 'ascii': '*', 'energy': 2}
		}
	
	def addTerrain(self, name, ascii, energy):
		self.terrain[len(self.terrain)] = {'name': name, 'ascii': ascii, 'energy': energy}

	def addObstacle(self, name, ascii, energy):
		self.obstacles[len(self.obstacles)] = {'name': name,'ascii': ascii, 'energy': energy}
		

