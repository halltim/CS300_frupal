import random

class Map:
	# The generated map is completely random given if the terrain generated is water, no obstacle will be placed
	# The map is generated into a 3D array, a 2D array of 3 layers
	#	layer 1, index 0: Terrain as an ID
	#	layer 2, index 1: Obstacles as an ID
	#	layer 3, index 2: Tile is visible as a boolean
	# The IDs from the terrain and obstacles and randomly grabbed from the terrain and obstacle arrays respectivly
	#
	# tiles is the class from tile.py, it's located in GameState
	# width and height are the desired width and height of the map and what will be generated
	# viewport is the dimension of the viewport (for now a square viewport, so we only need one side's length and use it for both)
	def __init__(self, tiles, width, height):
		self.arr = []
		self.width = width
		self.height = height
		for row in range(height):
			for col in range(width):
				tile = []
				terrain = random.randint(1, len(tiles.terrain) - 1)
				tile.append(terrain) 
				if terrain == 4: # Terrain is water
					tile.append(0)
				else:
					tile.append(random.randint(0, len(tiles.obstacles) - 1)) # obstacle
				tile.append(False) # visible
				self.arr.append(tile)
	
	def assertion(self, x, y):
		assert(y < self.height and x < self.width and x >= 0 and y >= 0)

	# Gets the terrain ID from (x,y)
	def getTerrain(self, x, y):
		self.assertion(x, y)
		return self.arr[y*self.width + x][0]
	
	# Gets the obstacle ID from (x,y)
	def getObstacle(self, x, y):
		self.assertion(x, y)
		return self.arr[y*self.width + x][1]
	
	# returns true if there is an obsticle
	def hasObstacle(self, x, y):
		self.assertion(x, y)
		return self.arr[y*self.width + x][1] > 0

	def removeObstacle(self, x, y):
		self.assertion(x, y)
		self.arr[y*self.width + x][1] = 0
	
	# True if tile is visible
	def isVisible(self, x, y):
		self.assertion(x, y)
		return self.arr[y*self.width + x][2] == 1
		
	def setVisible(self, x, y):
		self.assertion(x, y)
		self.arr[y*self.width + x][2] = 1

	# sets all tiles visible
	def revealMap(self):
		for y in range(self.height):
			for x in range(self.width):
				self.arr[y*self.width + x][2] = True
